## DevDependencies

### ts-node:
- ts-node is a TypeScript execution engine and REPL for Node.js.
- We can can run .ts files with nodemon, when ts-node is installed. Cheers !!

### typescript:
- TypeScript Compiler.

### pg:
- required to use postgres with typeorm

### Babel To use non-relative imports 
- @babel/core
- @babel/cli
- @babel/preset-env 
- babel-plugin-module-resolver
- @babel/preset-typescript

Note: Babel does not use typescript package at all. Test: Delete typescript packages, i think they're just for ide warnings
@babel/preset-typescript just adds support for ts files.

# reflect-metdata
- Error:
- : Column type for World#name is not defined and cannot be guessed. Make sure you have turned on an "emitDecoratorMetadata": true option in tsconfig.json. Also make sure you have imported "reflect-metadata" on top of the main entry file in your application (before any entity imported).If you are using JavaScript instead of TypeScript you must explicitly provide a column type.
