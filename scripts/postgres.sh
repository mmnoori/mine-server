  
  # Connect to minecraft database 
  psql -U admin -d minecraft

  # Drop Database
  psql -U admin -d postgres -c "DROP DATABASE minecraft;"
  
  # Drop Table
  DROP TABLE world;