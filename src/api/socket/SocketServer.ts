import { Server as HttpServer } from 'http'
import { Server as SocketIOServer } from 'socket.io'

interface ServerToClientEvents {
  noArg: () => void
  basicEmit: (a: number, b: string, c: Buffer) => void
  withAck: (d: string, callback: (e: number) => void) => void
}

interface ClientToServerEvents {
  hello: () => void
}

interface InterServerEvents {
  ping: () => void
}

interface SocketData {
  name: string
  age: number
}

export default class SocketServer {
  private io: SocketIOServer

  constructor(httpServer: HttpServer) {
    this.io = new SocketIOServer<
      ClientToServerEvents,
      ServerToClientEvents,
      InterServerEvents,
      SocketData
    >(httpServer, {
      cors: {
        origin: '*',
        methods: ['GET', 'POST'],
      },
    })

    this.io.on('connection', (socket) => {
      console.log('A client connected')

      socket.on('message', (msg) => {
        console.log('Message received: ', msg)
        socket.emit('message', `Server received: ${msg}`)
      })

      socket.on('disconnect', () => {
        console.log('A client disconnected')
      })
    })
  }
}
