import amqp, { Channel, Connection } from 'amqplib'

class RabbitMQ {
  private static instance: RabbitMQ
  private connection!: Connection
  private channel!: Channel
  private assertedQueues: Set<string> = new Set()

  private constructor() {}

  public static getInstance(): RabbitMQ {
    if (!RabbitMQ.instance) {
      RabbitMQ.instance = new RabbitMQ()
    }
    return RabbitMQ.instance
  }

  public async connect(url: string): Promise<void> {
    if (!this.connection) {
      this.connection = await amqp.connect(url)
      this.channel = await this.connection.createChannel()
      console.log('RabbitMQ connected')

      this.connection.on('error', (err: any) =>
        console.error('Rabbit connection error', err)
      )
      this.connection.on('close', () => console.log('Rabbit connection closed'))
    }
  }

  public getChannel(): Channel {
    if (!this.channel) {
      throw new Error(
        'RabbitMQ channel is not initialized. Call connect() first.'
      )
    }
    return this.channel
  }

  public async assertQueue(
    queue: string,
    options = {
      durable: true, // Ensures the queue will survive a broker restart
      presistent: true, // the message will be stored on disk rather than just in memory, providing a higher level of reliability
    }
  ): Promise<void> {
    if (!this.assertedQueues.has(queue)) {
      // Ensures that a queue with the specified name exists and creates it if it does not.
      await this.channel.assertQueue(queue, options)
      this.assertedQueues.add(queue)
      console.log(`Queue "${queue}" asserted`)
    }
  }

  // Test RabbitMQ RPC
  public async assertReplyQueue() {
    const correlationId = generateCorrelationId()
    const replyQueue = await this.channel.assertQueue('', { exclusive: true })

    this.channel.consume(
      replyQueue.queue,
      (msg) => {
        if (msg && msg.properties.correlationId === correlationId) {
          console.log('Server is Now Started:', msg.content.toString())
          // setTimeout(() => {
          //   this.connection.close()
          // }, 500)
        }
      },
      { noAck: true }
    )

    this.channel.sendToQueue('start_server_queue', Buffer.from('start'), {
      correlationId: correlationId,
      replyTo: replyQueue.queue,
    })
  }
}

function generateCorrelationId() {
  return (
    Math.random().toString() +
    Math.random().toString() +
    Math.random().toString()
  )
}

export default RabbitMQ
