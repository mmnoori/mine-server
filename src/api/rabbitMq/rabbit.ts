import {
  generateRandomPort,
  generateRandomString,
} from '../../utils/fakeHelper'
import RabbitMQ from './RabbitMQ'

export async function sendMsg({ queue = 'testqueue', message = {} }) {
  queue = 'testqueue'
  message = {
    host: `${generateRandomString(10)}`,
    port: generateRandomPort(),
    date: new Date(),
  }

  RabbitMQ.getInstance()
    .getChannel()
    .sendToQueue(queue, Buffer.from(JSON.stringify(message)))
  console.log(`Sent message to queue "${queue}": ${message}`)
}

export async function sendRPCMessage({ queue = 'testqueue', message = {} }) {
  queue = 'testqueue'
  message = {
    host: `${generateRandomString(10)}`,
    port: generateRandomPort(),
    date: new Date(),
  }

  RabbitMQ.getInstance()
    .getChannel()
    .sendToQueue(queue, Buffer.from(JSON.stringify(message)))
  console.log(`Sent message to queue "${queue}": ${message}`)
}
