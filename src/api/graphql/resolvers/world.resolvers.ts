// Third-Parties
import { GraphQLError } from 'graphql'

// Types
// import { Resolvers } from '$__generated__/resolvers-types'
import { Resolvers } from '../../../__generated__/resolvers-types'

// Database
// import { userService } from '$database/services/user.service'
import { worldService } from '../../../database/services/world.service'
import { userService } from '../../../database/services/user.service'
import { sendMsg } from '../../rabbitMq/rabbit'

export const worldResolvers: Resolvers = {
  Mutation: {
    // Works
    async createWorld(parent, { input }, context) {
      return await worldService.createWorld(input.world, input.version)
    },
    async runWorld() {
      await sendMsg({})

      return { message: 'assdasd' }
    },
    // async deleteWorld() {
    //   await sendMsg({})

    //   return { message: 'assdasd' }
    // },
  },

  Query: {
    // async getWorlds(parent, {}, context) {
    // async getWorlds(parent, {}, context) {
    //     return userService.getUsers()
    //     // return []
    // },
  },
}
