export function getRandomInteger(min: number = 0, max: number = 1000): number {
  // Calculate a random number between min and max, both inclusive
  return Math.floor(Math.random() * (max - min + 1)) + min
}

function getLength(input: string | number) {
  if (typeof input === 'string') 
    return input.length;
}

export function generateRandomPort() {
  const minPort = 49152
  const maxPort = 65535
  return Math.floor(Math.random() * (maxPort - minPort + 1)) + minPort
}

export function generateRandomString(length: number) {
  const characters =
    'abcdefghijklmnopqrstuvwxyz0123456789'
  let result = ''

  for (let i = 0; i < length; i++) {
    const randomIndex = Math.floor(Math.random() * characters.length)
    result += characters[randomIndex]
  }

  return result
}