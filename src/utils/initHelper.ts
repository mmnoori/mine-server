// Third-Parties
import { createClient } from 'redis'
import amqp, { Connection, Channel } from 'amqplib'

// Types
import { Server as HttpServer } from 'http'


// import mongoose from 'mongoose'

// import { AppDataSource } from '$database/app-data.source'
import { AppDataSource } from '../database/app-data.source'
import RabbitMQ from '../api/rabbitMq/RabbitMQ'
import SocketServer from '../api/socket/SocketServer'

export async function initRedis() {
  // Create a Redis client
  const client = await createClient({
    // url: 'redis://minecraft-redis:6379',
    socket: {
      host: 'minecraft-redis',
      port: 6379,
    },
  })
    .on('error', (err) => console.log('Redis Client Error', err))
    .connect()

  client.set('someKey', 158).then(() => {
    client.get('someKey').then((value) => {
      // console.log(`value`)
      // console.log(value)
    })
  })
}

export async function initMongoDB() {
  // const connection = await mongoose.connect('mongodb://minecraft-mongo/minedb')
  // console.log(`connection`)
  // console.log(connection)
  // console.log(`connection.connection`)
  // console.log(connection.connection)
  // const small = new User({ name: 'world-test' })
  // await small.save()
  // User.find().then((res) => {
  //   console.log(`res`)
  //   console.log(res)
  // })
}

export async function initPostgresDB() {
  await AppDataSource.initialize()
}

export async function initRabbitMQ() {
  await RabbitMQ.getInstance().connect('amqp://minecraft-rabbitmq')

  // await RabbitMQ.getInstance().assertQueue('testqueue')
  await RabbitMQ.getInstance().assertReplyQueue()
}

export function initSocketIo(httpServer: HttpServer) {
  new SocketServer(httpServer)
}