import Docker from 'dockerode'
import { Socket } from 'socket.io'

const docker = new Docker({ socketPath: '/var/run/docker.sock' })

async function createAndStartContainer() {
    // Create the container
    const container = await docker.createContainer({
        Image: 'itzg/minecraft-server:java17',
        name: 'minecraft-docker-mc-1',
        Tty: true,
        OpenStdin: true,
        HostConfig: {
            PortBindings: {
                '25565/tcp': [
                    {
                        HostPort: '25565'
                    }
                ]
            }
        },
        Env: [
            'EULA=TRUE',
            'INIT_MEMORY=512M',
            'MAX_MEMORY=1512M',
            'ONLINE_MODE=false',
            'DIFFICULTY=hard',
            'MAX_PLAYERS=5',
            'TYPE=VANILLA',
            'VERSION=1.20.1'
        ]
    });

    console.log('Container created successfully.');

    // Start the container
    await container.start();
    console.log('Container started successfully.');
}

async function runMinecraftServer() {
  try {
      // Pull the Minecraft server image
      await docker.pull('itzg/minecraft-server:java17', (err, stream) => {
          if (err) {
              console.error('Error pulling image:', err);
              return;
          }

          docker.modem.followProgress(stream, onFinished, onProgress);

          function onFinished(err, output) {
              if (err) {
                  console.error('Error:', err);
              } else {
                  console.log('Image pulled successfully.');
                  createAndStartContainer();
              }
          }

          function onProgress(event) {
              console.log('Progress:', event.status);
          }
      });
  
  } catch (error) {
      console.error('Error:', error);
  }
}

runMinecraftServer();


// docker.listContainers(function (err, containers) {
//   if (err) return console.log('error  docker')
//   if (containers)
//     containers.forEach(function (containerInfo) {
//       // docker.getContainer(containerInfo.Id).stop(cb);
//       console.log(`containerInfo`)
//       console.log(containerInfo)
//     })
// })

// image wont be downloaded if not exists
// docker.run('hello-world').then(() => {
//   console.log('runned hello world')
// })

// docker.pull('hello-world:latest', function (err, stream) {
//   // streaming output from pull...
//   console.log('hello-world image pulled')
// });
