const amqp = require('amqplib')

const init = async () => {
  const connection = await amqp.connect('amqp://minecraft-rabbitmq')

  const channel = await connection.createChannel()

  const queue = 'testqueue'

  channel.assertQueue(queue, {
    durable: true,
  })

  console.log(' [*] Waiting for messages in %s. To exit press CTRL+C', queue)

  channel.consume(
    queue,
    function (msg) {
      console.log(' [x] Received %s', msg.content.toString())
    },
    {
      noAck: true,
    }
  )
}

init()

setInterval(() => {
  console.log('Keeping the program running...');
}, 10000); // Log every second
