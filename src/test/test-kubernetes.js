const k8s = require('@kubernetes/client-node')

const kc = new k8s.KubeConfig()
kc.loadFromDefault()

console.log(`k8s.CoreV1Api`)
console.log(k8s.CoreV1Api)

const k8sApi = kc.makeApiClient(k8s.CoreV1Api)

const main = async () => {
  try {
    console.log('Getting list of pods ...')
    const podsRes = await k8sApi.listNamespacedPod('default')
    const nameSpaces = await k8sApi.listNamespace()
    const allServices = await k8sApi.listServiceForAllNamespaces()

    // const res = await k8sApi.createNamespacedPod('default', podManifest);
    // console.log('Pod created:', res.body);

    console.log(`podsRes.body`)
    console.log(podsRes.body)
    // console.log(podsRes.body)
    // console.log(`nameSpaces.body`)
    // console.log(nameSpaces.body)
    // console.log(`allServices.body`)
    // console.log(allServices.body)
  } catch (err) {
    console.error(err)
  }
}

main()
