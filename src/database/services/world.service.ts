// Third-Parties
import { In, Repository } from 'typeorm'
import { GraphQLError } from 'graphql'
import { validateOrReject, ValidationError } from 'class-validator'
import bcrypt from 'bcrypt'

// Model
// import { World } from '$database/entities/world.entity'
import { World } from '../entities/world.entity'

// Graphql
// import { Resolvers } from '$__generated__/resolvers-types'

// Database
// import { AppDataSource } from '$database/app-data.source'
import { AppDataSource } from '../app-data.source'
import { userService } from './user.service'

class WorldService {
    constructor(public worldRepository: Repository<World>) {}

    getRepository() {
        return this.worldRepository
    }

    async getAllWorlds(userId: number) {
        const queryBuilder = this.worldRepository.createQueryBuilder('room')

        return await queryBuilder
            .innerJoin('room.users', 'u')
            .where('"u"."id" = :id', { id: userId })
            .getMany()
    }

    // async addMessageToRoom(roomId: number, message: Message, io?: Server) {
    //   const queryBuilder = this.worldRepository.createQueryBuilder()
    //   await queryBuilder
    //     .update(Room, {
    //       messages: () => `messages || '${JSON.stringify(message)}'::jsonb`,
    //     })
    //     .where('id = :id', { id: roomId })
    //     .execute()

    //   if (io) {
    //     io.to(`${roomId}`).emit('message', {
    //       message,
    //       roomId,
    //     })
    //   }

    //   return await this.worldRepository.findOne({
    //     where: { id: roomId },
    //     relations: ['users'],
    //   })
    // }

    async createWorld(world: String, version: String) {
        // const newWorld = this.worldRepository.create({
        //   name: world,
        //   version: version,
        // })

        // return await this.worldRepository.save(newWorld)

        return {
            name: 'hehe',
            version: 'asdasdasd',
        }
    }

    async CreateWorldForExistingUser() {
        // const newWorld = this.worldRepository.create({
        //   name: 'test-world-1',
        //   version: '1.16.1',
        //   datapacks: ['datapack1', 'datapack2'],
        // })

        const worlds = this.worldRepository.find()

        // const users = await userService.findByIds([])
        const users = await userService.getRepository().find()

        console.log(`users`)
        console.log(users)
    }

    // async findRoomWithUsersId(recieverId: number, senderId: number) {
    //   const queryBuilder = this.worldRepository.createQueryBuilder('room')

    //   const rooms = await queryBuilder
    //     .select()
    //     .innerJoin('room.users', 'u')
    //     .where('"u"."id" = :senderId', { senderId })
    //     .getMany()

    //   return !!rooms?.some((room) => room.users.some((u) => u.id === recieverId))
    // }
}

export const worldService = new WorldService(AppDataSource.getRepository(World))
