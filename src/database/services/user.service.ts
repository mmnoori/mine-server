// Third-Parties
import { In, Repository } from 'typeorm'
import { GraphQLError } from 'graphql'
import { validateOrReject, ValidationError } from 'class-validator'
import bcrypt from 'bcrypt'

// Model
// import { User } from '$database/entities/user.entity'
import { User } from '../entities/user.entity'

// Graphql
// import { SignupInput } from '$__generated__/resolvers-types'
import { SignupInput } from '../../__generated__/resolvers-types'

// Database
// import { AppDataSource } from '$database/app-data.source'
import { AppDataSource } from '../app-data.source'

class UserService {
    constructor(public userRepository: Repository<User>) {}

    getRepository() {
        return this.userRepository
    }

    // async validateRequest() {

    // }

    async create(signupInput: SignupInput) {
        const user = this.userRepository.create(signupInput)

        return await validateOrReject(user)
            .then(async () => {
                const password = await bcrypt.hash(signupInput.password, 10)
                user.password = password

                return await this.userRepository.save(user)
            })
            .catch((errors: ValidationError[]) => {
                throw new GraphQLError('validation error', {
                    extensions: {
                        errors,
                        code: 'BAD_USER_INPUT',
                    },
                })
            })
    }

    async findOneByEmail(email: string) {
        // only if entity.column({ select: false })
        const user = await this.userRepository
            .createQueryBuilder('user')
            .select('user')
            .addSelect('user.password')
            .where('email = :email', { email })
            .getOne()

        return user
    }

    async findByIds(ids: Array<number>) {
        return await this.userRepository.findBy({ id: In(ids) })
    }

    async getUsers() {
        return [
            {
                id: 1564564564,
                email: 'asdasfsdfsdf',
                firstName: 'asdsdfdsf',
                lastName: 'noori',
            },
            {
                id: 1564564564,
                email: 'asdasfsdfsdf',
                firstName: 'asdsdfdsf',
                lastName: 'noori',
            },
        ]
    }
}

export const userService = new UserService(AppDataSource.getRepository(User))
