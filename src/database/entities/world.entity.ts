// Third-Parties
import {
    Entity,
    Column,
    PrimaryGeneratedColumn,
    ManyToOne,
    Generated,
    JoinTable,
    JoinColumn,
    ManyToMany
} from 'typeorm'
import { IsEmail, MinLength } from 'class-validator'

// Types
import { Difficulty, Software_Types } from '../../__generated__/resolvers-types'

// Model
import { User } from './user.entity'

@Entity()
export class World {
    @PrimaryGeneratedColumn()
    id: number

    // @PrimaryGeneratedColumn("uuid")
    // id: string;

    @Column()
    @Generated("uuid")
    uuid: string

    @Column()
    name: string

    @Column()
    version: string

    @Column({
        type: 'enum',
        enum: Difficulty,
        default: Difficulty.Easy,
    })
    difficulty: Difficulty

    @Column({
        type: 'enum',
        enum: Software_Types,
        default: Software_Types.Spigot,
    })
    serverType: Software_Types

    @Column({ default: true })
    pvp: boolean
    
    @Column({ default: 4 })
    maxPlayers: number

    @Column('simple-array', { default: [] })
    datapacks: string[]

    @ManyToOne(() => User)
    @JoinColumn({ name: "ownerId" }) // Define Column Name
    owner: User

    @ManyToMany(() => User)
    @JoinTable()
    members: User[]
}
