// Third-Parties
import {
    Entity,
    Column,
    PrimaryGeneratedColumn,
    ManyToMany,
    JoinTable,
    OneToMany,
    JoinColumn,
    Generated,
} from 'typeorm'
import { IsEmail, MinLength } from 'class-validator'

// Model
import { World } from './world.entity'

@Entity()
export class User {
    @PrimaryGeneratedColumn()
    id: number

    // Use uuid as primary key
    // @PrimaryGeneratedColumn("uuid")
    // id: string;

    @Column()
    @Generated("uuid")
    uuid: string

    @Column()
    firstName: string

    @Column()
    lastName: string

    // @Column({ nullable: true })
    // newCol: string

    @Column({ nullable: false, unique: true  })
    @IsEmail()
    email: string

    @Column()
    @MinLength(6)
    password: string

    @OneToMany(() => World, (world) => world.owner)
    worlds: World[]

    // @Column('jsonb', { nullable: false })
    // messages: Message[]
}
