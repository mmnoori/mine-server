import { DataSource } from 'typeorm'

export const AppDataSource = new DataSource({
    type: 'postgres',
    // host: 'localhost',
    host: 'minecraft-postgres',
    port: 5432,
    username: 'admin',
    password: 'admin',
    database: 'minecraft',
    // entities: ['./src/**/*.entity.ts'],
    entities: ['./dist/**/*.entity.js'],
    // entities: ['./babel-output/**/*.entity.js'],
    logging: false,
    synchronize: true,
    // synchronize: false,
    // migrations: ['./dist/database/migrations/*-migrate.js'],
    // migrationsRun: true
})
