// Database
import { getRandomInteger } from '../../utils/fakeHelper'
import { User } from '../entities/user.entity'
import { World } from '../entities/world.entity'
import { userService } from '../services/user.service'
import { worldService } from '../services/world.service'

const worldRepository = worldService.getRepository()
const userRepository = userService.getRepository()

export const testCreateUsers = async () => {
    const existingUsers = await userRepository.find()

    if (existingUsers.length) return console.log('Users Already Existed ...')

    const newUsers = []
    const entryCount = 50

    for (let i = 0; i < entryCount; i++) {
        const user = userRepository.create({
            firstName: `firstName-${i}`,
            lastName: `lastName-${i}`,
            email: `user@gmail.com${i}`,
            password: `passwordddddd${i}`,
        })

        newUsers.push(user)

        await userRepository.save(user)
    }

    console.log('Users Created !!!')
}

export const testCreateWorlds = async () => {
    const existingWorlds = await worldRepository.find()

    // console.log(`existingWorlds[0]`)
    // console.log(existingWorlds[0])

    if (existingWorlds.length) return console.log('Worlds Already Existed ...')

    const newWorlds = []
    const entryCount = 50

    for (let i = 0; i < entryCount; i++) {
        const world = worldRepository.create({
            name: `test-world-${i}`,
            version: `1.16.${i}`,
            datapacks: [`datapack${i}`, `datapack${i}`],
            owner: await getRandomUser(),
        })

        await worldRepository.save(world)

        // console.log(`\n`)
        // console.log(`Saved this world:`)
        // console.log(world)
    }

    console.log('\n')
    console.log('Worlds Created !!!')
}

export const addMemberForWorlds = async () => {
    const worlds = await worldRepository.find()

    for (const world of worlds) {
        const users = []

        users[0] = await getRandomUser()
        users[1] = await getRandomUser()

        world.members = users

        // console.log(`world.id: ${world.id}`)
        // console.log(`users[0].id: ${users[0].id}`)
        // console.log(`users[1].id: ${users[1].id}`)

        await worldRepository.save(world)
        console.log(`Added members to world ${world.name}`)
    }
}

export const getRandomWorld = async () => {
    const worlds = await worldRepository.find({
        relations: {
            owner: true,
        },
    })

    const randomWorldIndex = getRandomInteger(0, worlds.length - 1)

    const randomWorld = worlds[randomWorldIndex]

    // if (randomWorld.owner) {
    //     console.log(
    //         `${randomWorld.name} already had owner ${randomWorld.owner.id} !`
    //     )
    //     continue
    // }
    return randomWorld
}

export const getRandomUser = async () => {
    const users = await userRepository.find({
        // relations: {
        //     worlds: true,
        // },
    })

    const randomIndex = getRandomInteger(0, users.length - 1)

    const randomUser = users[randomIndex]

    return randomUser
}

// Fetch worlds that have owner
// const worldsHaveOwner = await worldRepository
// .createQueryBuilder('world')
// .innerJoinAndSelect('world.owner', 'owner')
// .getMany()

// console.log(`worldsHaveOwner`)
// console.log(worldsHaveOwner)
// return

// Fetch users and populate their worlds
// const usersWithWorlds = await userRepository
//   .createQueryBuilder('user')
//   .innerJoinAndSelect('user.worlds', 'world')
//   .getMany()

// console.log(`usersWithWorlds`)
// console.log(usersWithWorlds)
// console.log(usersWithWorlds[0].worlds)
// return
