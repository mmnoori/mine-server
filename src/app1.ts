// Third-Parties
// npm install @apollo/server express graphql cors
import * as dotenv from 'dotenv'
dotenv.config()

import 'reflect-metadata'

import { ApolloServer } from '@apollo/server'
import { expressMiddleware } from '@apollo/server/express4'
import { ApolloServerPluginDrainHttpServer } from '@apollo/server/plugin/drainHttpServer'
import express from 'express'
import http from 'http'
import cors from 'cors'
import { readFileSync } from 'fs'
import { mergeResolvers } from '@graphql-tools/merge'

// import { ExpressContext } from 'apollo-server-express'
// import { JwtPayload, Resolvers } from './__generated__/resolvers-types'

// Api
// import { worldResolvers } from '$api/graphql/resolvers/world.resolvers'
import { worldResolvers } from './api/graphql/resolvers/world.resolvers'
import { userResolvers } from './api/graphql/resolvers/user.resolvers'
// import { userResolvers } from '$api/graphql/resolvers/user.resolvers'

// Utils
import {
  initMongoDB,
  initPostgresDB,
  initRabbitMQ,
  initRedis,
  initSocketIo,
} from './utils/initHelper'
import {
  addMemberForWorlds,
  testCreateUsers,
  testCreateWorlds,
} from './database/test/db-test'

export interface MyContext {
  token?: string
  //   currentUser: JwtPayload
  //   authorized: boolean
  //   io: Server
}

// export interface MyContext extends ExpressContext {
//   currentUser: JwtPayload
//   authorized: boolean
//   io: Server
// }

const typeDefs = readFileSync('./schema.graphql', { encoding: 'utf-8' })

// Required logic for integrating with Express
const app = express()
// Our httpServer handles incoming requests to our Express app.
// Below, we tell Apollo Server to "drain" this httpServer,
// enabling our servers to shut down gracefully.
const httpServer = http.createServer(app)

const mergedResolvers = mergeResolvers([userResolvers, worldResolvers])

// Same ApolloServer initialization as before, plus the drain plugin
// for our httpServer.
const server = new ApolloServer<MyContext>({
  typeDefs,
  resolvers: mergedResolvers,
  plugins: [ApolloServerPluginDrainHttpServer({ httpServer })],
})

const init = async () => {
  await initRedis()
  await initMongoDB()
  await initPostgresDB()
  await initRabbitMQ()
  await initSocketIo(httpServer)

  // Ensure we wait for our server to start
  await server.start()

  await testCreateUsers()
  await testCreateWorlds()
  // await addMemberForWorlds()

  // Set up our Express middleware to handle CORS, body parsing,
  // and our expressMiddleware function.
  app.use(
    '/graphql',
    cors<cors.CorsRequest>(),
    express.json(),
    // expressMiddleware accepts the same arguments:
    // an Apollo Server instance and optional configuration options
    expressMiddleware(server, {
      context: async ({ req }) => ({ token: req.headers.token }),
    })
  )
  app.get('/', (req, res) => {
    res.send('Hehe From Rest')
  })

  // Modified server startup
  await new Promise<void>((resolve) =>
    httpServer.listen({ port: 4000 }, resolve)
  )
  console.log(`🚀 Server ready at http://localhost:4000/`)
}

init()
