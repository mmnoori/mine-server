const path = require('path')
const fs = require('fs')
// const jsConfig = require('./tsconfig.json')

// Define the path to your JSON file
const filePath = 'tsconfig.json' // Update with your file path

// try {
//   const data = fs.readFileSync(filePath, 'utf8')

// //   // Parse the JSON data into a JavaScript object
//   // const jsonObject = JSON.parse(data)
// //   console.log('Parsed JSON object:', jsonObject)
// } catch (err) {
//   console.error('Error reading or parsing file:', err)
// }

module.exports = {
  presets: ['@babel/preset-env', '@babel/preset-typescript'],
  plugins: [
    // Plugin to resolve module imports
    [
      'module-resolver',
      {
        // root: ['./src'],
        alias: {
          // Works
          // "^@src/(.+)": "./src/\\1",

          $database: './src/database', // Works
          $api: './src/api', // Works
          $utils: './src/utils',
          $__generated__: './src/__generated__',
        },
      },
    ],

    "transform-decorators-legacy",
    "transform-class-properties"
  ],
}
